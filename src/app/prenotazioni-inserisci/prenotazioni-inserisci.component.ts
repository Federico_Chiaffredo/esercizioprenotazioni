import { Component, OnInit } from '@angular/core';
import { Prenotazione } from '../prenotazione';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-prenotazioni-inserisci',
  templateUrl: './prenotazioni-inserisci.component.html',
  styleUrls: ['./prenotazioni-inserisci.component.css']
})
export class PrenotazioniInserisciComponent implements OnInit {

input_nome="";
input_cognome="";
input_data="";
input_ora="";


  constructor(private rest:RestService) { }

  ngOnInit(): void {
  }

  inserisciPrenotazione(){
    let temp= new Prenotazione();
    temp.nome=this.input_nome;
    temp.cognome=this.input_cognome;
    temp.data=Date.parse(this.input_data+" "+this.input_ora+":00");

    this.rest.addPrenotazione(temp).subscribe(

      (successo)=>{
        console.log(successo);
      },
      (errore)=>{
        console.log(temp);
        console.log(errore);
      }
    );
    
  }

}
