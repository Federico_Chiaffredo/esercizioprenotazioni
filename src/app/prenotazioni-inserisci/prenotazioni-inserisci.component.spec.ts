import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrenotazioniInserisciComponent } from './prenotazioni-inserisci.component';

describe('PrenotazioniInserisciComponent', () => {
  let component: PrenotazioniInserisciComponent;
  let fixture: ComponentFixture<PrenotazioniInserisciComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrenotazioniInserisciComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrenotazioniInserisciComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
