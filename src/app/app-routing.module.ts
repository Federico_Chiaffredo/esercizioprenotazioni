import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrenotazioniInserisciComponent } from './prenotazioni-inserisci/prenotazioni-inserisci.component';
import { PrenotazioniListaComponent } from './prenotazioni-lista/prenotazioni-lista.component';

const routes: Routes = [
  {path:"",redirectTo:"prenotazioni/list", pathMatch:"full"},
  {path:"prenotazioni/insert", component: PrenotazioniInserisciComponent},
  {path:"prenotazioni/list", component: PrenotazioniListaComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
