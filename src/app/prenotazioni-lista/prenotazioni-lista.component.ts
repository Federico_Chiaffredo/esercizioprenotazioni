import { Component, OnInit } from '@angular/core';
import {Prenotazione} from '../prenotazione';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-prenotazioni-lista',
  templateUrl: './prenotazioni-lista.component.html',
  styleUrls: ['./prenotazioni-lista.component.css']
})
export class PrenotazioniListaComponent implements OnInit {

  elenco :any=[];

  constructor(private rest:RestService) { }

  ngOnInit(): void {
    this.rest.getAllPrenotazioni().subscribe(
      (successo)=>{
        this.elenco=successo;
        for(let i=0;i<this.elenco.length;i++){
          this.elenco[i].dataOra=new Date(this.elenco[i].dataOra);
        }
      },
      (errore)=>{
        console.log(errore);
      }
    );
  }

}
