import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrenotazioniListaComponent } from './prenotazioni-lista.component';

describe('PrenotazioniListaComponent', () => {
  let component: PrenotazioniListaComponent;
  let fixture: ComponentFixture<PrenotazioniListaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrenotazioniListaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrenotazioniListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
