import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

const endPoint="http://localhost:8080/prenotazioni/";
@Injectable({
  providedIn: 'root'
})

export class RestService {

  constructor(private http: HttpClient) { }

addPrenotazione(objPren: any){
  let headerCostum=new HttpHeaders;
  headerCostum=headerCostum.set('content-type','application/JSON');
  return this.http.post(endPoint+"inserisci",JSON.stringify(objPren),{headers: headerCostum});
}

getAllPrenotazioni(){
  return this.http.get(endPoint + "elenco");
}


}
